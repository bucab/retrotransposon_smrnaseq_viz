# Instructions

## Prerequisite

When running on SCC, load the R module version 3.5.2

```
module load R/3.5.2
```

The script requires the R packages `tidyverse` and `docopt` to be installed.
These modules are installed by default into the SCC R module above, but must
be installed manually if they are not already available in other environments.

## Quick start

First, clone this repo:

```
git clone https://bitbucket.org/bubioinformaticshub/retrotransposon_smrnaseq_viz.git
```

After ensuring the appropriate R packages are installed as described above,
from the root of the cloned repository run:

```
scripts/TE_viz_line_plot.R data/testing_TE.csv figs/
```

This command will read in the metadata from `data/testing_TE.csv`, which
contains paths for three TEs available on SCC, and will write out plots to the
destination directory `figs/`. If the destination folder does not exist, the
script will create it.

## Input format

The script accepts a single input argument that should be a csv file containing
metadata about the TE CPM files in 4 columns:

+ TE: The full name of the TE, such as **ALU:1-312**.
+ Sample: Sample's name, such as **C_0002**.
+ Status: Sample's status, such as **Huntingtons** or **Control**.
+ Path: the absolute path of the TPM file of the TE in that sample, such as **/path/to/project/root/human/HD_PD_project/miRNA/C_0002_TE/TE@ALU:1-312**.

The following is an example of such a file containing six samples, three
control and three Huntington, for the TE ALU:1-312.

```
TE,Sample,Status,Path
ALU:1-312,C_0002,Control,/path/to/cpm/C_0002_TE/TE@ALU:1-312
ALU:1-312,C_0003,Control,/path/to/cpm/C_0003_TE/TE@ALU:1-312
ALU:1-312,C_0004,Control,/path/to/cpm/C_0004_TE/TE@ALU:1-312
ALU:1-312,H_0001,Huntingtons,/path/to/cpm/H_0001_TE/TE@ALU:1-312
ALU:1-312,H_0002,Huntingtons,/path/to/cpm/H_0002_TE/TE@ALU:1-312
ALU:1-312,H_0003,Huntingtons,/path/to/cpm/H_0003_TE/TE@ALU:1-312
```

The script will output six plots from this file into the output directory:

```
ALU:1-312 any length line plot in Control.pdf
ALU:1-312 any length line plot in Huntingtons.pdf
ALU:1-312 length 18-23nt line plot in Control.pdf
ALU:1-312 length 18-23nt line plot in Huntingtons.pdf
ALU:1-312 length 24-35nt line plot in Control.pdf
ALU:1-312 length 24-35nt line plot in Huntingtons.pdf
```

i.e., one plot for each read length for each distinct value in the Status
column. Plots are grouped by Status, such that only rows with the given Status
value are included in the corresponding plot.

The script may contain multiple TEs. Plots like those above will be generated
for each.

## Help message

When running 
```
./TE_viz_line_plot.R -h
```
a help message will display on the screen, which says
```
Plot TPM distribution in the bins from the TE alignment data in different groups of samples.
Usage:
  TE_viz_line_plot.R <input_fn> <output_dir>

Options:
  -h --help  Show this screen.
  -v --version  Show version.

Arguments:
  input_fn  The absolute or relative path of the input file. Please see the READ.md for more details of the format of the file.
  output_dir  Output directory path either absolute or relative, for example "../figs" 
```

## Example plot

![Example plot](example_plot.png "Example plot")

An example output plot is shown above, with the file name exactly the same as
the figure title. The figure is in the pdf format. 

For a given TE, there will be three separate plots corresponding to three
different lengths: 18-23nt, 24-35nt and any length for each of the conditions
which include Control, Huntingtons and Parkinsons, depending on what data is in
the input file.

This information is summarised in the figure title and file name as well. For
instance, one output figure named `L1:1-5403 any length line plot in
Control.pdf` points out that this is a line plot with the reads of any lengths
in the Control samples for the TE L1:1-5403. 

In the example plot, two lines are drawn to showcase the median of the TPM data
from all of the selected samples in each bin, with red line representing
alignment to the positive strand and blue the negative strand. The shaded area
indicates the interquartile range in each group.









